from tasks.third_episode.class_example import Dog
from tests.test_third_episode.test_base import TestBase


class TestMyFeature(TestBase):

    @classmethod
    def setUpClass(cls):
        print("BEFORE ALL")

    def setUp(self):
        super().setUp()
        print("BEFORE TEST!")

    def test_first_functionality(self):
        poodle = Dog()
        self.log("Play with poodle")
        result = poodle.play()
        self.log("Verify playing result")
        assert result == "And now I'm playing"
        self.log("Verify now I'm playing with unittest")
        self.assertEqual(result, "And now I'm playing", "Invalid value")

    def test_other(self):
        self.log("Verify 1 equals 1")
        assert 1 == 1

    def tearDown(self):
        print("AFTER TEST!")

    @classmethod
    def tearDownClass(cls):
        print("AFTER ALL")



