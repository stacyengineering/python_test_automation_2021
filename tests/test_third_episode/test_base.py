import unittest

import logging


class TestBase(unittest.TestCase):

    def setUp(self):
        self._current_test_method_name = self._testMethodName
        self._step_num = 1

    def log(self, msg):
        print(f"({self._current_test_method_name}) Step #{self._step_num}: {msg}")
        self._step_num += 1
