import requests


class ApiClient:

    def __init__(self):
        self.url = "http://127.0.0.1:8087"

    def get_all(self, endpoint):
        return requests.get(f"{self.url}/{endpoint}")

    def get_with_params(self, endpoint, params):
        return requests.get(f"{self.url}/{endpoint}", params=params)

    def put(self, endpoint, body):
        pass

    def post(self, endpoint, resource_id, body):
        pass

    def delete(self, resource_id):
        pass
