import unittest

from tests.clients.api_client import ApiClient


class TestBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.api_client = ApiClient()
