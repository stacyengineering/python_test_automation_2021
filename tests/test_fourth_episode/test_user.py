from tests.test_fourth_episode.test_base import TestBase


class TestUser(TestBase):

    def test_get_user(self):
        result_for_all_users = self.api_client.get_all("users")
        self.assertEqual(result_for_all_users.status_code, 200)


