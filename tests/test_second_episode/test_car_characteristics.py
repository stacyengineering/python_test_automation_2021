import pytest

from tasks.second_episode.car import Car


@pytest.fixture()
def cars():
    return {
        "ford_fiesta": Car("Ford Fiesta", "blue"),
        "seat_leon": Car("Seat Leon", "green"),
        "kia_stinger": Car("Kia Stinger", "grey"),
    }


def test_create_cars():
    for car_name in ("Ford Fiesta", "Seat Leon", "Kia Stinger"):
        car_obj = Car(car_name, "blue")
        assert car_obj.model == car_name
        assert car_obj.price is not None
        assert car_obj.interior == {}
        assert car_obj.exterior == {}


def test_models(cars):
    assert cars["ford_fiesta"].model == "Ford Fiesta"
    assert cars["seat_leon"].model == "Seat Leon"
    assert cars["kia_stinger"].model == "Kia Stinger"


def test_price(cars):
    expected_values = {
        "ford_fiesta": 16000,
        "seat_leon": 21000,
        "kia_stinger": 35000
    }
    for car, exp_price in expected_values.items():
        assert cars[car].price == exp_price


def test_create_and_repaint_fiesta(cars):
    fiesta = cars["ford_fiesta"]
    assert fiesta.color == "blue"
    fiesta.repaint("red")
    assert fiesta.color == "red"

