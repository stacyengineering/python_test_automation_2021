
class Car:
    """
    Car class representing a car model.
    """
    def __init__(self, model, color, interior=None, exterior=None):
        """
        Car init, creating a new car instance.
        :param model (str): one of: Ford Fiesta, Seat Leon, Kia Stinger
        :param color (str): car color
        :param interior (dict): dictionary {}, representing car interior items. Optional. Example: {"wheel": "red"}
        :param exterior (dict): dictionary {}, representing car exterior items. Optional. Example: {"spoiler": "big"}
        """
        self.__model = model
        self.__color = color
        self.__price = None
        self.__interior = interior or {}
        self.__exterior = exterior or {}

    @property
    def model(self):
        """Get car model."""
        return self.__model

    @property
    def color(self):
        """Get car color."""
        return self.__color

    @property
    def price(self):
        """Get car price. If model is unknown, an exception will be thrown."""
        if self.model == "Ford Fiesta":
            self.__price = 16000
        elif self.model == "Seat Leon":
            self.__price = 21000
        elif self.model == "Kia Stinger":
            self.__price = 35000
        else:
            raise Exception("Model not found!")
        return self.__price

    def repaint(self, new_color):
        """Repaint the car :) """
        self.__color = new_color

    @property
    def interior(self):
        """Get current car interior items."""
        return self.__interior

    @property
    def exterior(self):
        """Get current car exterior itesm."""
        return self.__exterior

    def add_to_interior(self, new_interior_item, item_characteristic):
        """
        Add new item to interior
        :param new_interior_item: (str) name of interior item, for example, seat
        :param item_characteristic: (str) characteristic, for example, green
        """
        self.__interior[new_interior_item] = item_characteristic

    def add_to_exterior(self, new_exterior_item, item_characteristic):
        """
        Add new item to exterior.
        :param new_exterior_item: (str) name of exterior item, for example, wheel
        :param item_characteristic: (str) characteristic of exterior item, for example, metal
        """
        self.__exterior[new_exterior_item] = item_characteristic
