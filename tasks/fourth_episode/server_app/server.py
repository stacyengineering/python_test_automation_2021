#!/usr/bin/env python

import cherrypy
import os

from src import context
from src.routes.index import Index

# todo: fix this:
root = f'{os.path.dirname(os.path.abspath(os.getcwd()))}/frontend-app/front-app/src/dist'  # this file's directory

conf = {
    '/': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': root,
        'tools.staticdir.index': 'index.html',
        'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    }
}


def configure_and_start(cont):
    print("Starting prod server")
    cherrypy.server.socket_host = cont.host_name
    cherrypy.server.socket_port = cont.port_num
    cherrypy.tree.mount(Index(cont), '/', conf)
    cherrypy.engine.start()
    cherrypy.engine.block()
    return cherrypy.engine


if __name__ == "__main__":
    # todo: handle dynamic port
    configure_and_start(context.Context())

