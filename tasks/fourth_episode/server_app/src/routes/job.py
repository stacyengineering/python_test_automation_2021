import cherrypy

from tasks.fourth_episode.server_app.src.db.users_model import NotFoundException


@cherrypy.popargs("job_id")
class Job:
    exposed = True

    def __init__(self, cont):
        self._cont = cont

    @cherrypy.tools.json_out()
    def GET(self, job_id=None):
        try:
            if not job_id:
                return self._cont.jobs.get_all()
            return self._cont.jobs.get_by_id(job_id)
        except NotFoundException:
            cherrypy.response.status = 404
            return {"reason": "{} not found".format(job_id)}

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self):
        job_data = cherrypy.request.json
        return {"id": self._cont.jobs.create(job_data)}

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self, job_id):
        job_data = cherrypy.request.json
        return {"id": self._cont.users.update(job_id, job_data)}

    @cherrypy.tools.json_out()
    def DELETE(self, job_id):
        try:
            self._cont.jobs.delete(job_id)
        except NotFoundException:
            cherrypy.response.status = 404
            return {"reason": "{} not found".format(job_id)}
