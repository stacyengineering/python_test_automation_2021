from tasks.fourth_episode.server_app.src.routes.job import Job
from tasks.fourth_episode.server_app.src.routes.user import User


class Index(object):
    exposed = True

    def __init__(self, cont):
        self.users = User(cont)
        self.jobs = Job(cont)
