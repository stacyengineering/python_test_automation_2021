import cherrypy

from tasks.fourth_episode.server_app.src.db.users_model import NotFoundException


@cherrypy.popargs("user_id")
class User:
    exposed = True

    def __init__(self, cont):
        self._cont = cont

    @cherrypy.tools.json_out()
    def GET(self, user_id=None):
        try:
            if not user_id:
                return self._cont.users.get_all()
            return self._cont.users.get_by_id(user_id)
        except NotFoundException:
            cherrypy.response.status = 404
            return {"reason": "{} not found".format(user_id)}

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self):
        user_data = cherrypy.request.json
        return {"id": self._cont.users.create(user_data)}

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self, user_id):
        user_data = cherrypy.request.json
        return {"id": self._cont.users.update(user_id, user_data)}

    @cherrypy.tools.json_out()
    def DELETE(self, user_id):
        try:
            self._cont.users.delete(user_id)
        except NotFoundException:
            cherrypy.response.status = 404
            return {"reason": "{} not found".format(user_id)}
