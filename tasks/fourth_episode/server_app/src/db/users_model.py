import uuid

from tasks.fourth_episode.server_app.src.db.base_model import BaseModel


class NotFoundException(Exception):
    pass


class UsersModel(BaseModel):
    def __verify_data(self, data):
        if "name" not in data:
            raise Exception("Name must be provided")
        elif "surname" not in data:
            raise Exception("Surname must be provided")
        elif "age" not in data:
            raise Exception("Age must be provided")
    #
    # def create(self, data):
    #     self.__verify_data(data)
    #     user_id = str(uuid.uuid4())
    #     data["id"] = user_id
    #     self.__users.append(data)
    #     return user_id
    #
    # def update(self, user_id, data):
    #     found_user = [user for user in self.__users if user["id"] == user_id][0]
    #     for key, value in data.items():
    #         found_user[key] = value
    #     return found_user
    #
    # def delete(self, user_id):
    #     for user in self.__users:
    #         if user["id"] == user_id:
    #             self.__users.remove(user)
    #             return
    #     raise NotFoundException()
    #
    # def get_all(self):
    #     return self.__users
    #
    # def get_by_id(self, user_id):
    #     for user in self.__users:
    #         if user["id"] == user_id:
    #             return user
    #     raise NotFoundException()


u = UsersModel()
user_id = u.create({"name": "Bob", "surname": "Sinclair", "age": 81})
print(u.get_all())
print(u.get_by_id(user_id))
u.update(user_id, {"name": "Lilly"})
print(u.get_by_id(user_id))
# u.delete(user_id)
# print(u.get_all())