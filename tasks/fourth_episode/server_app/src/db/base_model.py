import uuid

from abc import abstractmethod


class NotFoundException(Exception):
    pass


class BaseModel:
    def __init__(self):
        self.__resources = []

    @abstractmethod
    def __verify_data(self, data):
        pass

    def create(self, data):
        self.__verify_data(data)
        resource_id = str(uuid.uuid4())
        data["id"] = resource_id
        self.__resources.append(data)
        return resource_id

    def update(self, resource_id, data):
        found_resource = [res for res in self.__resources if res["id"] == resource_id][0]
        for key, value in data.items():
            found_resource[key] = value
        return found_resource

    def delete(self, resource_id):
        for resource in self.__resources:
            if resource["id"] == resource_id:
                self.__resources.remove(resource)
                return
        raise NotFoundException()

    def get_all(self):
        return self.__resources

    def get_by_id(self, resource_id):
        for resource in self.__resources:
            if resource["id"] == resource_id:
                return resource
        raise NotFoundException()
#
#
# u = BaseModel()
# user_id = u.create({"name": "Bob", "surname": "Sinclair", "age": 81})
# print(u.get_all())
# print(u.get_by_id(user_id))
# u.update(user_id, {"name": "Lilly"})
# print(u.get_by_id(user_id))
# u.delete(user_id)
# print(u.get_all())