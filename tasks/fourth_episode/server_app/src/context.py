import json
import os

from tasks.fourth_episode.server_app.src.db.jobs import JobsModel
from tasks.fourth_episode.server_app.src.db.users_model import UsersModel


class Context(object):
    def __init__(self):
        """
        Configuration manager.
        """
        config = "{}/configuration/dev_config.json".format(
            os.path.dirname(os.path.dirname(__file__)))

        with open(config) as fl:
            self.settings = json.loads(fl.read())
            self.host_name = self.settings["host_name"]
            self.port_num = self.settings["port_num"]
            self.users = UsersModel()
            self.jobs = JobsModel()
