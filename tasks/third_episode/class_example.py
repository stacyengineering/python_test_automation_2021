

class Animal:
    class_attr = 1

    def __init__(self, max_speed):
        print(f"Max speed from Animal: {max_speed}")
        self.max_speed = max_speed
        self.min_speed = 0

    def __speed_checker(self, speed):
        if speed > self.max_speed:
            raise Exception("Speed exceeded!")

    def run(self, speed):
        self.__speed_checker(speed)
        print(f"Is running with speed of {speed} km/h")

    @classmethod
    def class_method(cls):
        print(f"THIS IS CLASS METHOD: {cls.min_speed}")


class Dog(Animal):
    def __init__(self):
        super().__init__(30)

    def run(self):
        print("I'm running")
        super(Dog, self).run(6)

    def play(self):
        self.run()
        return "And now I'm playing"

# dog.run(100)
# dog = Animal(50)
# cat = Animal(50)
# dog.class_attr = 2
# Animal.class_attr = 3
# dog.max_speed = 7
# # print(cat.class_attr)
# # print(Animal.class_attr)
# print(dog.max_speed)
# print(cat.max_speed)
