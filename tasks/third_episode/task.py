import pathlib


class AuthError(Exception):
    message = "Credentials are incorrect!"

    def __init__(self):
        super().__init__(self.message)


class Unauthorized(Exception):
    message = "Unauthorized! Please, login!"

    def __init__(self):
        super().__init__(self.message)


class IOError(Exception):
    message = "File already exists!"

    def __init__(self):
        super().__init__(self.message)


class FileManager:
    success_message = "Success"

    def __init__(self):
        self.__logged_in = False
        self.__username = "john.doe"
        self.__password = "J0hNd0e"

    def login(self, username, password):
        """
        Log in to file manager

        :param username: (str) username
        :param password: (str) password
        :return: "Success"
        """
        if username != self.__username or password != self.__password:
            raise AuthError()
        self.__logged_in = True
        return self.success_message

    def logout(self):
        """
        Logout from file manager

        :return: "Success"
        """
        self.__logged_in = False
        return self.success_message

    def __verify_login(self):
        if not self.__logged_in:
            raise Unauthorized()

    def __get_file_path(self, file_name, file_path):
        return pathlib.Path(file_path).absolute() / file_name

    def create(self, file_name, file_path):
        """
        Create new file given a name and path

        :param file_name: (str) name of the file including extension.
        :param file_path: (str or Path object) path to file not containing file name. If you want to create the file
            in same directory, just pass "." as file_path parameter value.
        :return: "Success"
        """
        self.__verify_login()
        path_to_file = self.__get_file_path(file_name, file_path)
        if path_to_file.exists():
            raise IOError()
        open(path_to_file, "w")
        return self.success_message

    def delete(self, file_path):
        """
        Delete file by given file_path.

        :param file_path: (str) path to file.
        :return: "Success"
        """
        self.__verify_login()
        pathlib.Path(file_path).unlink()
        return self.success_message

    def write_to(self, file_name, content, file_path="."):
        """
        Write content to file. Each time you use this function on one file, content will be added to the end
        of current content. No new lines will be included automatically.

        :param file_name: (str) name of the file including extension.
        :param content: (str) content to be added to the file.
        :param file_path: [OPTIONAL] (str) path to file. By default, current directory.
        :return: "Success"
        """
        self.__verify_login()
        path_to_file = self.__get_file_path(file_name, file_path)
        with open(path_to_file, "a") as opened_file:
            opened_file.write(content)
        return self.success_message

    def read_from(self, file_name, file_path="."):
        """
        Read conten fromt he file and return it.

        :param file_name: (str) name of the file including extension.
        :param file_path: [OPTIONAL] (str) path to file. By default, current directory.
        :return: (str) file content.
        """
        self.__verify_login()
        path_to_file = self.__get_file_path(file_name, file_path)
        with open(path_to_file, "r") as opened_file:
            return opened_file.read()
