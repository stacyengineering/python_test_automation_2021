import json
import pathlib


def login(credentials):
    """
    Login to the system using your username and password.

    :param credentials: JSON, username and password credentials,
                        for example: { "username": "john", "password": "j0Hn" }
    :return:
        - Success: "Your login was successful. Welcome, <username>!"
        - Failure: "Cannot login, please retry. Error: <error message>."
    """
    error = "Cannot login, please retry. Error: {}."

    if type(credentials) == str:
        credentials = json.loads(credentials)

    if "username" not in credentials:
        return error.format("Please, provide username")
    if "password" not in credentials:
        raise error.format("Please, provide password")

    print("Logging in...")
    return f"Your login was successful. Welcome, {credentials['username']}!"


def create_file(file_name, path):
    """
    Create file with name to given path.

    :param file_name: str, name of the file.
    :param path: str, path where to save.

    :return:
        - Success: "File was saved."
        - Failure: "File was not saved. Error: <error message>"
    """
    error_msg = "File was not saved. Error: {}"
    path_to_store = pathlib.Path(path).absolute()
    if not path_to_store.exists():
        return error_msg.format("Path to store file doesn't exist.")

    path_to_file = path_to_store / file_name
    if path_to_file.exists():
        return error_msg.format("File already exists.")

    with open(path_to_file, "w"):
        return "File was saved."


def write_to_file(file_path, content):
    """
    Write text to file.

    :param file_path: str, path to file including it's name.
    :param content: str content of file.
    :return:
        - Success: "Content saved."
        - Failure: "Error saving content: <error>.
    """
    error_msg = "Error saving content: {}."
    path_to_file = pathlib.Path(file_path)
    if not path_to_file.exists():
        return error_msg.format(f"File doesn't exist under path: {file_path}.")

    with open(path_to_file, "w") as f:
        f.write(content)
        return "Content saved."


def sum_numbers(*args):
    """
    Get sum of given numbers.

    :param args: int or float.
    :return:
        - int or float result sum.

    If a value is incorrect, there will be an exception.
    """
    return sum(args)
